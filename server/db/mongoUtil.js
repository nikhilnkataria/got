module.exports = {
  mostActiveAttackerKing: (col) => {
    return col.aggregate([
      { $group  : { _id: "$attacker_king", count: {$sum: 1} } },
      { $sort   : { count : -1 } },
      { $limit  : 1 }
    ]).toArray().then(function(documents) {
      return documents;
    });
  },
  mostActiveDefenderKing: (col) => {
    return col.aggregate([
      { $group  : { _id: "$defender_king", count: {$sum: 1} } },
      { $sort   : { count : -1 } },
      { $limit  : 1 }
    ]).toArray().then(function(documents) {
      return documents;
    });
  },
  mostActiveRegion : (col) => {
    return col.aggregate([
      { $group  : { _id: "$region", count: {$sum: 1} } },
      { $sort   : { count : -1 } },
      { $limit  : 1 }
    ]).toArray().then(function(documents) {
      return documents;
    });
  },
  totalWin: (col) => {
    return col.find({ "attacker_outcome":"win" }).toArray().then(function(documents) {
      return documents;
    });
  },
  totalLoss: (col) => {
    return col.find({ "attacker_outcome":"loss" }).toArray().then(function(documents) {
      return documents;
    });
  },
  avgDefenderSize: (col) => {
    return col.aggregate([
      { $group  : { _id: {}, count: {$avg: "$defender_size"} } },
      { $sort   : { count : 1 } },
      { $limit  : 1 }
    ]).toArray().then(function(documents) {
      return documents;
    });
  },
  minDefenderSize: (col) => {
    return col.aggregate([
      { $group  : { _id: {}, count: {$min: "$defender_size"} } },
      { $sort   : { count : 1 } },
      { $limit  : 1 }
    ]).toArray().then(function(documents) {
      return documents;
    });
  },
  maxDefenderSize: (col) => {
    return col.aggregate([
      { $group  : { _id: {}, count: {$max: "$defender_size"} } },
      { $sort   : { count : 1 } },
      { $limit  : 1 }
    ]).toArray().then(function(documents) {
      return documents;
    });
  },
  uniqueBattleTypes: (col) => {
    return col.distinct("battle_type").then((documents) => {
      return documents;
    });;
  },
  getSearchObj: (query) => {
    var findObj = {};
    findObj['$or'] = [];
    if(query.king !== undefined && query.king.length > 0) {
      findObj['$or'].push({attacker_king: {$regex : query.king}});
      findObj['$or'].push({defender_king: {$regex : query.king}});
    }

    if(query.commander !== undefined && query.commander.length > 0) {
      findObj['$or'].push({attacker_commander: {$regex : query.commander}});
      findObj['$or'].push({defender_commander: {$regex : query.commander}});
    }

    if(query.location !== undefined && query.location.length > 0) {
      findObj.location = query.location;
    }

    if(query.type !== undefined && query.type.length > 0) {
      findObj.battle_type = query.type;
    }

    if(query.region !== undefined && query.region.length > 0) {
      findObj.region = query.region;
    }

    if(query.battle_type !== undefined && query.battle_type.length > 0) {
      findObj.battle_type = query.battle_type;
    }

    if(query.major_death !== undefined && query.major_death.length > 0) {
      findObj.major_death = query.major_death;
    }

    if(findObj['$or'].length === 0) {
        delete findObj['$or'];
    }

    return findObj;
  }
};
