const express = require('express');
const expressMongoDb = require('express-mongo-db');
//const async = require('async');

const {APP_CONSTANTS} = require('../global/constants');
const port = process.env.PORT || APP_CONSTANTS.serverPort;
const mongoUtil = require('./db/mongoUtil');
const app = express();

app.use(expressMongoDb(APP_CONSTANTS.mongoDburl));

app.get('/list', (req, res) => {
    var col = req.db.collection('wars');
    col.distinct("location", ((err, locations) => {
        var jsonObj = {
            status: 0,
            msg: 'Unable to fetch locations'
        };
        if (err) {
            return res.json(jsonObj);
        }

        jsonObj = {
            status: 1,
            msg: 'Success',
            locations: locations
        };

        return res.json(jsonObj);
    }));
});

app.get('/stats', (req, res) => {
    var col = req.db.collection('wars');
    const getStatus = async () => {
        let activeAttackerKing = await mongoUtil.mostActiveAttackerKing(col);
        let activeAttackerKing1 = await mongoUtil.mostActiveDefenderKing(col);
        let activeRegion = await mongoUtil.mostActiveRegion(col);
        let win = await mongoUtil.totalWin(col);
        let loss = await mongoUtil.totalLoss(col);
        let minDSize = await mongoUtil.minDefenderSize(col);
        let maxDSize = await mongoUtil.maxDefenderSize(col);
        let avgDSize = await mongoUtil.avgDefenderSize(col);
        let battleTypes = await mongoUtil.uniqueBattleTypes(col);

        let jsonObj = {
            'status': 1,
            'msg': 'Success',
            most_active: {
                attacker_king: (activeAttackerKing.length > 0) ? activeAttackerKing[0]._id : 'Not Found',
                defender_king: (activeAttackerKing1.length > 0) ? activeAttackerKing1[0]._id : 'Not Found',
                region: (activeAttackerKing1.length > 0) ? activeRegion[0]._id : 'Not Found'
            },
            attacker_outcome: {
                'win': win.length,
                'loss': loss.length
            },
            battle_type: battleTypes,
            defender_size: {
                'average': (avgDSize.length > 0) ? avgDSize[0].count.toFixed(2) : 'Not Found',
                'min': (minDSize.length > 0) ? minDSize[0].count : 'Not Found',
                'max': (maxDSize.length > 0) ? maxDSize[0].count : 'Not Found'
            }
        };
        res.json(jsonObj);
    };

    getStatus();
});

app.get('/count', (req, res) => {
    var col = req.db.collection('wars');
    col.find().count((err, count) => {
        var jsonObj = {
            status: 0,
            msg: 'Unable to fetch data'
        };
        if (err) {
            return res.json(jsonObj);
        }

        jsonObj = {
            status: 1,
            msg: 'Success',
            count: count
        };

        return res.json(jsonObj);
    });
});

app.get('/search', (req, res) => {
    var findObj = mongoUtil.getSearchObj(req.query);
    var col = req.db.collection('wars');
    col.find(findObj, {_id: 0, name: 1}).toArray((err, items) => {
        return res.json({
            'status': 1,
            'msg': 'Success',
            battles: items
        });
    });
});

app.get('/', (req, res) => {
    res.json({
        'status': 0,
        'msg': 'Invalid Method'
    });
});

app.listen(port, (err, resp) => {
    if (err) {
        return console.log('Could not connect to server');
    }
    console.log(`Server started at port ${port}`);
});
